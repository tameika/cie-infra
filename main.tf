provider "aws" {
	region = "${var.region}"
}

resource "aws_subnet" "cie-dev-public" {
  vpc_id     = "${aws_vpc.cie-dev.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "cie-dev-public"
  }
}

resource "aws_subnet" "cie-dev-private" {
  vpc_id     = "${aws_vpc.cie-dev.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "cie-dev-private"
  }
}

resource "aws_vpc" "cie-dev" {
  #create_vpc = true
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  instance_tenancy = "default"
  #name = "cie-vpc"
  #private_subnets = ["${aws_subnet.cie-dev-private.id}"]
  #public_subnets = ["${aws_subnet.cie-dev-public.id}"]
  tags {
    Name = "cie-dev"
  }
}

resource "aws_security_group" "cie-sg" {
    name = "vpc_rules"
    description = "Allow traffic to pass from the private subnet to the internet"
    vpc_id = "${aws_vpc.cie-dev.id}"    
    
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
}


resource "aws_instance" "cieami" {
	ami = "ami-6871a115"
	instance_type = "${var.instance_type}"
	associate_public_ip_address = true
	availability_zone = "us-east-1a"
	subnet_id = "${aws_subnet.cie-dev-public.id}"
#	security_groups = ["${aws_security_group.cie-sg.id}"]
	count = 2
	vpc_security_group_ids = ["${aws_security_group.cie-sg.id}"]
}

#resource "aws_instance" "cieami" {
#	ami = "ami-fad25980"
#	instance_type = "${var.instance_type}" 
#	associate_public_ip_address =  true
#	subnet_id = "${aws_subnet.cie-dev-public.id}"
#	security_groups = ["${aws_security_group.cie-sg.id}"]
#	tags {
#    Name = "cie-dev-insta"
#  }
#}

#resource "aws_eip" "base" {
#        instance = "${aws_instance.cieami.id}"
#        vpc = true
#}

