provider "aws" {
	region = "${var.region}"
}

resource "aws_subnet" "cie-dev-public" {
  vpc_id     = "${aws_vpc.cie-dev.id}"
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "cie-dev-public"
  }
}

resource "aws_subnet" "cie-dev-private" {
  vpc_id     = "${aws_vpc.cie-dev.id}"
  cidr_block = "10.0.3.0/24"
  availability_zone = "us-east-1a"
  enable_dns_hostnames = true

  tags {
    Name = "cie-dev-private"
  }
}

resource "aws_vpc" "cie-dev" {
  #create_vpc = true
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  instance_tenancy = "default"
  #name = "cie-vpc"
  #private_subnets = ["${aws_subnet.cie-dev-private.id}"]
  #public_subnets = ["${aws_subnet.cie-dev-public.id}"]
  tags {
    Name = "cie-dev"
  }
}
