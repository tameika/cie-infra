variable "region" {
	description = "Default region for ecs"
	default = "us-east-1"
}

variable "ami" {
	description = "Default ami based on region"
	type = "map"
	default = {
		us-east-1 = "ami-6871a1150"
#		us-east-2 = "ami-58f5db3d"
	}
}


variable "cluster_name" {
	type = "string"
	default = "cie-dev"
}

variable "instance_type" {
	description = "the instance type"
	default = "t2.small"
}

