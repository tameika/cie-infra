provider "aws" {
	region = "${var.region}"
}


resource "aws_security_group" "cie-sg" {
    name = "vpc_rules"
    description = "Allow traffic to pass from the private subnet to the internet"
    vpc_id = "${aws_vpc.cie-dev.id}"    
    
    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] 
    }   
    ingress {
        from_port = 443
        to_port = 443 
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress {
        from_port = 3306
        to_port = 3306
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
   }
}
